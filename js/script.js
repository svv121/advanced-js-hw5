"use strict";
/*
*/

/*--------------------------Cards-----------------------------------*/
class Card {
    constructor(name, email, title, body, id) {
        this.name = name;
        this.email = email.toLowerCase();
        this.title = title.toUpperCase();
        this.body = body;
        this.id = id;
        this.sectionContainer = document.querySelector('section.container');
    }

    createPost() {
        const cardForm = document.createElement('div');
        cardForm.classList.add('card-form');
        cardForm.id = this.id;
        cardForm.innerHTML =
            `<span class="entry">Name: </span><span class="name">${this.name}</span>
             <p class="email"><span class="entry">email: </span><a href="mailto:${this.email}">${this.email}</a></p>
             <button class="post-delete-button">delete</button>
             <h3 class="post-title"><span class="entry">Title: </span>${this.title}</h3>
             <p class="post-text"><span class="entry">Text: </span>${this.body}</p>`;
        this.sectionContainer.append(cardForm);
    }

    deletePost(id) {
        const deletePost = async (event) => {
            const deletePostBtn = event.target.closest('.post-delete-button');
            if (deletePostBtn === null) return;
            const cardToDelete = deletePostBtn.parentElement;
            const postId = cardToDelete.id;
            if (+postId === id) {
                await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
                    method: 'DELETE'
                });
                cardToDelete.remove()
            }
        }
        this.sectionContainer.addEventListener('click', deletePost);
    }
}
/*---------------------------Promises-----------------------------------*/
const getUsersPromise = fetch('https://ajax.test-danit.com/api/json/users', {
    method: 'GET'})
    .then(res => res.json())
    .catch(err => console.warn(err))

const getPostsPromise = fetch('https://ajax.test-danit.com/api/json/posts', {
    method: 'GET'})
    .then(res => res.json())
    .catch(err => console.warn(err))

getUsersPromise.then(users => console.log(users))
getPostsPromise.then(posts => console.log(posts))

Promise.all([getUsersPromise, getPostsPromise])
    .then(([users, posts]) => {
    const usersWithPosts = users.map(user => {
        let postsWithSameUserId = [];
        posts.forEach(post => {
           const {userId} = post;
           if (userId === user.id) {
               postsWithSameUserId.push(post);
           }
        })
        user.posts = postsWithSameUserId;
        return user;
    })

    usersWithPosts.forEach(({ name, email, posts }) => {
        posts.forEach(post => {
            post = new Card(name, email, post.title, post.body, post.id);
            post.createPost();
            const {id} = post;
            post.deletePost(id);
        })
    })
})